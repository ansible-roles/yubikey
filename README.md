# Ansible role for installing Software, Libraries and scripts for handling Yubikey security token

Installs needed software packages and libraries and downloads the [repository with scripts for handling and provisioning Yubikeys](https://gitlab.mis.mpg.de/scripts/yubikey).

## Prerequirements

* none

## Variables

* none

